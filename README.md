# Evil Corporation Datacenter

Jest to klaster [k3s](https://k3s.io) zainstalowany na trzech Raspberry Pi, został przygotowany przeze mnie specjalnie na **PHPers Warszawa #18**.

W związku z tym, iż k3s niestety nie ma niektórych funkcji związanych z bezpieczeństwem (PodSecurityPolicy), a wszyscy korzystamy z konta z podwyższonymi uprawnieniami, uprzejmię proszę o grzeczną zabawę i niepsucie klastra - pamiętajcie, że inni też się chcą pobawić :).

### Włam się do WiFi złej korporacji

Aby zalogować się do WiFi, użyj następujących danych:

* SSID: evilcorp-secure-network
* Klucz: secure123

**Bardzo ważne** sieć korzysta z serwera DNS, który znajduje się w klastrze. Teoretycznie inni użytkownicy klastra mogą widzieć Twoje zapytania DNS, z uwagi na Twoją prywatność nie korzystaj z wrażliwych usług internetowych!

### Instalacja kubectl

Aby skorzystać z funkcji klastra, należy zainstalować na swoim komputerze najnowszą wersję narzędzia `kubectl`.
Proces instalacji opisany jest [tutaj](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

### Konfiguracja

Utwórz nowy plik o nazwie `config` w katalogu `.kube` w swoim katalogu domowym i wklej do niego poniższy kod yaml:

`apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM1RENDQWN5Z0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFqTVJBd0RnWURWUVFLRXdkck0zTXQKYjNKbk1ROHdEUVlEVlFRREV3WnJNM010WTJFd0hoY05NVGt3TmpJeE1UTXdNREF4V2hjTk1qa3dOakU0TVRNdwpNREF4V2pBak1SQXdEZ1lEVlFRS0V3ZHJNM010YjNKbk1ROHdEUVlEVlFRREV3WnJNM010WTJFd2dnRWlNQTBHCkNTcUdTSWIzRFFFQkFRVUFBNElCRHdBd2dnRUtBb0lCQVFEYnpNdE9WbTcrNEN3Nmk0NFZneldWbVpacUJHNUwKeDRYaFZubzJtc3l5WEdUeDBMNkdLaEloejFHUXBRbEpuKzVCVVM5ZnhYUEJSUy94bXp3Skx4NEVxQ2xOUHpOWApYeU56dGp0enc3c0dtYWRMN1FETlNFZ3RsV3luTnR3Y0xBUnpjVFRDWXhEK3FPUlV2MkdCRVFqaDBkSlpkMk16Clp6cGp3SkpLd1M0WlIxYi9hS1hZSFhpYXBQaFJiVTUzMGRqL0gvTjdQRVN3TXU0UVVWNFFGN29yYmhWclhzQU8KdUdUemI2RGFpL3JJTWc4QTl0NnhqbmlOZ1p2Z3N0VDVwMVU1VmV0Yk9lUGpSYmJtd3NLdmE5Wi9sN2RUazZVMwpTUm4yak9QT1JQekVvd29rbDN1czlWOUJFckZvMUY4L3M3LzB4aTZiRmRuNTFNTHl5SHdHdEJjNUFnTUJBQUdqCkl6QWhNQTRHQTFVZER3RUIvd1FFQXdJQ3BEQVBCZ05WSFJNQkFmOEVCVEFEQVFIL01BMEdDU3FHU0liM0RRRUIKQ3dVQUE0SUJBUUJOOTZ5ekgxcmwrU2JxelFLMnBEQURhUm1sZ3NTZHNsYkFJVnN4c3RZTlZmcm5xaWFPY2FvYgpDN1VjL2Ryck5oZGZWZktBL1l2OG5qa0ppR1VoMG9TL0tEMnFxcFlGQXJPMExDU0JUUXlqQVkrc3BhT2VVQ1plCktUMndOWVBpSnNJalpjMWI5L0wyTDVOZlhkcVh2RTkxd0huV2JhQVhHY0lUeUFhRS9aYkV2NzEySHZhV1p0YnUKRlhOaFVaWjEwb3BIbDJIeHhEcllEaUxYd0tRWjJ0b2F6NWNXQW1EQ3ZTVTEyejBjOVN0ajROWGozZEpqOXNBcAorV1hKYml6Z2k2dEVNdjBtU0FBQTNHWVg3NXFrdEhuU1luQS82SXJJM0l4N2xMbUNDQ1dJaHNoWFVlSzBsbG42CkdpeXZoWEFSeGRMcXV3ZHdUSGNBTlVrUGNrMTFxTnFNCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
    server: https://192.168.2.10:6443
  name: default
contexts:
- context:
    cluster: default
    namespace: evilcorp #zastąp tę wartość swoją przestrzenią
    user: default
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: default
  user:
    password: 25dd1119292009fe9dbeb5551a3ea1cf
    username: admin`

Po więcej informacji [zajrzyj do oficjalnej dokumentacji](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/)).

### Utwórz swoją przestrzeń

Żeby nie wchodzić w konflikty z innymi osobami korzystającymi z klastra, utwórz swoją przestrzeń - dzięki temu zasoby o tych samych nazwach będą mogły współistnieć w różnych przestrzeniach.
Aby utworzyć przestrzeń wykonaj poniższą komendę:

`kubectl create namespace twoja-nazwa`

Teraz zastąp w pliku .kube/config nazwę przestrzeni `evilcorp` na tę, którą utworzyłeś.

Po zakończeniu zabawy z klastrem proszę, usuń swoją przestrzeń wykonując polecenie `kubectl delete namespace twoja-nazwa`.

Pamiętaj o tym, aby nie modyfikować zasobów w innych przestrzeniach - możesz w ten sposób uszkodzić klaster lub przeszkodzić innym użytkownikom.

### Wystawianie usług

Spróbuj testowo uruchomić jakąś usługę. Niech to będzie na przykład serwer nginx. W terminalu uruchom następującą komendę:

```bash
$ kubectl create deployment my-nginx --image=nginx
```

gdzie `my-nginx` to nazwa obiektu typu `Deployment`, a parametr `--image=nginx` oznacza, że usługa zostanie uruchomiona na podstawie [tego](https://hub.docker.com/_/nginx) obrazu.
W efekcie powinien pokazać się następujący komunikat:

```
deployment.apps/my-nginx created
```

Sprawdź teraz status swojego deploymentu uruchamiając polecenie:

```bash
$ kubectl get all
```

to polecenie pobiera wszystkie obiekty z aktualnego namespace'a. Powinno to wyglądać mniej więcej tak:

```
NAME                            READY   STATUS              RESTARTS   AGE
pod/my-nginx-5d998f947f-mxkhn   0/1     ContainerCreating   0          1s

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/my-nginx   0/1     1            0           1s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/my-nginx-5d998f947f   1         1         0       1s
```

powtórz to kilka razy, aż zobaczysz, że usługa została uruchomiona. Powinno to zostać zasygnalizowane zmianą statusu poda na `Running` i liczby w kolumnie `AVAILABLE` w wierszu deploymentu na 1.

Wspaniale! Usługa działa. Teraz powinniśmy się do niej dostać. Możemy to zrobić na kilka sposobów, tutaj omówimy dwa z nich:

#### LoadBalancer

Najprostszym sposobem będzie wystawienie usługi w sieci pod osobnym adresem IP. Aby to zrobić, należy wykonać polecenie:

```bash
$ kubectl expose deployment my-nginx --port=80 --type="LoadBalancer"
```

powinno zostać potwierdzone przez komunikat:

```
service/my-nginx exposed
```

Teraz zobaczmy jak wygląda nasza przestrzeń poprzez ponowne pobranie wszystkich zasobów:

```bash
$ kubectl get all
```

Efekt powinien być podobny do tego:

```
NAME                            READY   STATUS    RESTARTS   AGE
pod/my-nginx-5d998f947f-mxkhn   1/1     Running   0          6m28s

NAME               TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/my-nginx   LoadBalancer   10.43.176.160   192.168.3.2   80:31165/TCP   103s

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/my-nginx   1/1     1            1           6m29s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/my-nginx-5d998f947f   1         1         1       6m29s
```

Rzeczywiście powstał jakiś serwis! A cóż to za adres w kolumnie `EXTERNAL-IP`? Sprawdź to wpisując go w pasku adresu przeglądarki (oczywiście ten, który wyświetlił się Tobie)!

Czary? Nie, to usługa MetalLB, możesz poczytać o tym więcej tutaj: [https://metallb.universe.tf](https://metallb.universe.tf).

#### Ingress

Drugim sposobem jest tzw. ingress. Obiekt ten dodaje regułę do zbiorczego loadbalancera, którego rolę w przypadku k3s pełni [Traefik](https://traefik.io).
Aby to zrobić, utwórz plik `my-nginx-ingress.yml` i wejdź z poziomu terminala do katalogu, w którym ten plik się znajduje.
Ingress selektorem wskazuje serwis, poniższy szablon zawiera również definicję serwisu dla tego obiektu.
Następnie wklej do tego pliku następujący kod:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: nginx
spec:
  rules:
  - host: nginx.7ru-h4x0r.phpers # Pamiętaj o tym żeby zmienić ten adres!
    http:
      paths:
      - backend:
          serviceName: nginx-service-for-ingress
          servicePort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-for-ingress
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  type: ClusterIP
```

Adres `nginx.jakas-nazwa.phpers` powinien zamiast części `jakas-nazwa` zawierać wybrany przez Ciebie adres (szkoda by było utworzyć identyczny adres, jak ktoś inny).

Co ciekawe, polecenie `kubectl get all` nie zwróci nam tego zasobu. Żeby pobrać zasoby typu ingress, należy wykonać polecenie:

```bash
$ kubectl get ingress
```

Sprawdź teraz podany przez siebie adres w przeglądarce. Powinna być pod nim widoczna strona demonstracyjna serwera Nginx.

### OpenFAAS

W klastrze znajduje się zainstalowana usługa OpenFAAS.
Aby uruchomić w niej swoją funkcję, zainstaluj narzędzie faas-cli zgodnie z [instrukcją w repozytorium narzędzia](https://github.com/openfaas/faas-cli).
Aby pobrać szablony OpenFAAS dla PHP dla procesorów ARM, uruchom komendę:
```bash
$ faas-cli template pull https://gitlab.com/matul/openfaas-templates.git
```

W celu utworzenia nowej funkcji PHP uruchom
```bash
$ faas-cli new --lang php7-armhf moja-nowa-funkcja
```
Pamiętaj o tym, żeby dać unikalną nazwę!

Pojawią się pliki związane z Twoją funkcją. W pliku yml ustaw wartość `gateway` na `http://192.168.2.10:31112`.
Edytuj swoją funkcję do woli. Gdy uznasz, że funkcja jest gotowa, zbuduj ją i zdeployuj:

```bash
faas-cli build -f ./moja-nowa-funkcja.yml
faas-cli deploy -f ./moja-nowa-funkcja.yml
```

Interfejs OpenFAAS dostępny jest [tutaj](http://192.168.2.10:31112/ui/).

### Problemy, pytania

Złap mnie, kręcę się gdzieś po obiekcie ;)

# Miłej zabawy!